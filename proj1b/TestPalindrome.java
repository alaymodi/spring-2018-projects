import org.junit.Test;
import static org.junit.Assert.*;

public class TestPalindrome {
    // You must use this palindrome, and not instantiate
    // new Palindromes, or the autograder might be upset.
    static Palindrome palindrome = new Palindrome();

    @Test
    public void testWordToDeque() {
        Deque d = palindrome.wordToDeque("persiflage");
        String actual = "";
        for (int i = 0; i < "persiflage".length(); i++) {
            actual += d.removeFirst();
        }
        assertEquals("persiflage", actual);
    }

    @Test
    public void testIsPalindrome() {
        assertTrue(palindrome.isPalindrome("racecar"));
        assertFalse(palindrome.isPalindrome("Racecar"));
        assertTrue(palindrome.isPalindrome("12321"));
        assertFalse(palindrome.isPalindrome("1222212"));
        assertFalse(palindrome.isPalindrome("cat"));
        assertTrue(palindrome.isPalindrome("e"));
        assertTrue(palindrome.isPalindrome(""));
        assertTrue(palindrome.isPalindrome("r a c e c a r"));

    }

    @Test
    public void testsOffByone() {
        OffByOne off = new OffByOne();
        assertTrue(palindrome.isPalindrome("qbdecar", off));
        assertTrue(palindrome.isPalindrome("QbdecaR", off));
        assertTrue(palindrome.isPalindrome("QBDECAR", off));
        assertTrue(palindrome.isPalindrome("FlakE", off));
        assertTrue(palindrome.isPalindrome("", off));
        assertTrue(palindrome.isPalindrome("a", off));
        assertTrue(palindrome.isPalindrome("11322", off));
        assertFalse(palindrome.isPalindrome("Flake", off));
        assertFalse(palindrome.isPalindrome("!!@", off));
        assertFalse(palindrome.isPalindrome("11112", off));
        assertFalse(palindrome.isPalindrome("Qdbecar", off));
        assertFalse(palindrome.isPalindrome("dad", off));
    }
}
