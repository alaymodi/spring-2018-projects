public class Palindrome {

    public Deque<Character> wordToDeque(String word) {
        Deque<Character> str = new ArrayDeque<Character>();
        for (int x = 0; x < word.length(); x++) {
            str.addLast(word.charAt(x));

        }
        return str;
    }

    public boolean isPalindrome(String word) {
        Deque<Character> wordPal = wordToDeque(word);
        int x = 0;
        while (x < wordPal.size() / 2) {
            if (!(wordPal.removeFirst()).equals(wordPal.removeLast())) {
                return false;
            }
        }
        return true;
    }

    public boolean isPalindrome(String word, CharacterComparator cc) {
        Palindrome drome = new Palindrome();
        Deque<Character> wordPal = drome.wordToDeque(word);
        int x = 0;
        while (x < wordPal.size() / 2) {
            if (!(cc.equalChars(wordPal.removeFirst(), wordPal.removeLast()))) {
                return false;
            }
        }
        return true;
    }
}
