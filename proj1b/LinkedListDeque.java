public class LinkedListDeque<T> {
    private static class Node<T> {

        private T item;
        private Node next;
        private Node previous;

        Node(T i, Node n, Node p) {
            item = i;
            next = n;
            previous = p;
        }
    }

    private Node<T> sentinel;
    private int size;

    public LinkedListDeque() {
        sentinel = new Node(null, null, null);
        sentinel.previous = sentinel;
        sentinel.next = sentinel.previous;
        size = 0;
    }

    public void addFirst(T item) {
        Node temp = sentinel.next;
        sentinel.next = new Node(item, temp, sentinel);
        temp.previous = sentinel.next;
        size += 1;
    }

    public void addLast(T item) {
        Node temp = sentinel.previous;
        sentinel.previous = new Node(item, sentinel, temp);
        temp.next = sentinel.previous;
        size += 1;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void printDeque() {
        Node temp = sentinel.next;
        while (temp != sentinel) {
            System.out.print(temp.item + " ");
            temp = temp.next;
        }
    }

    public T removeFirst() {
        if (isEmpty()) {
            return null;
        }
        Node removed = sentinel.next;
        removed.next.previous = sentinel;
        sentinel.next = removed.next;
        removed.next = null;
        removed.previous = null;
        size -= 1;
        return (T) removed.item;
    }

    public T removeLast() {
        if (isEmpty()) {
            return null;
        }
        Node removed = sentinel.previous;
        T item = (T) removed.item;
        removed.previous.next = sentinel;
        sentinel.previous = removed.previous;
        removed.next = null;
        removed.previous = null;
        size -= 1;
        return item;
    }

    private boolean indexOutOfBonds(int index) {
        return index < 0 || index >= size;
    }

    public T get(int index) {
        if (indexOutOfBonds(index)) {
            return null;
        }
        Node temp = sentinel.next;
        for (int x = 0; x < index; x++) {
            temp = temp.next;
        }
        return (T) temp.item;
    }

    public T getRecursive(int index) {
        if (indexOutOfBonds(index)) {
            return null;
        }
        Node temp = sentinel.next;
        if (index > size - 1) {
            return null;
        }
        return (T) getIt(temp, index);

    }

    private T getIt(Node<T> n, int index) {
        if (index == 0) {
            return n.item;
        } else {
            return (T) getIt(n.next, index - 1);
        }
    }

}
