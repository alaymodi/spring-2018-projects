public class ArrayDeque<T> {

    private int size;
    private T[] myArr;
    private int nextFirst;
    private int nextLast;

    public ArrayDeque() {
        myArr = (T[]) new Object[8];
        nextFirst = myArr.length - 1;
        nextLast = 0;
        size = 0;
    }

    public void addFirst(T item) {
        resize();
        myArr[nextFirst] = item;
        nextFirst = (nextFirst - 1) % myArr.length;
        size += 1;
    }

    public void addLast(T item) {
        resize();
        myArr[nextLast] = item;
        nextLast = (nextLast + 1) % myArr.length;
        size += 1;
    }

    private boolean isTooBig() {
        double usagRatio = ((double) size()) /  ((double) myArr.length);
        return usagRatio < 0.25;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void resize() {
        if (size() == myArr.length) {
            expand();
        } else if (isTooBig() && myArr.length > 8) {
            contract();
        }
    }

    private void contract() {
        T[] arr = (T[]) new Object[myArr.length /  2];
        int index = 0;
        for (; index < arr.length / 2; index++) {
            arr[index] = get(index);
        }
        nextLast = size();
        myArr = arr;
        nextFirst = myArr.length - 1;

    }

    private void expand() {
        T[] arr = (T[]) new Object[size * 2];
        int index = 0;
        for (; index < size(); index++) {
            arr[index] = get(index);
        }
        nextLast = myArr.length;
        myArr = arr;
        nextFirst = myArr.length - 1;
    }

    public int size() {
        return size;
    }

    public void printDeque() {
        int start = nextFirst + 1;
        for (int count = 0; count < size(); count++) {
            if (start == myArr.length) {
                start = 0;
            }
            System.out.print(myArr[start] + " ");
            start += 1;
        }
    }

    public T removeFirst() {
        if (size() == 0) {
            return null;
        }
        int index = nextFirst + 1;
        if (index >= myArr.length) {
            index = 0;
        }
        T item = myArr[index];
        myArr[index] = null;
        nextFirst = index;
        size -= 1;
        resize();
        return item;
    }

    public T removeLast() {
        if (size() == 0) {
            return null;
        }
        if (size() == 1) {
            return removeFirst();
        }
        int index = nextLast - 1;
        if (nextLast - 1 < 0) {
            index = myArr.length - 1;
        }
        T item = myArr[index];
        myArr[index] = null;
        nextLast = index;
        size -= 1;
        resize();
        return item;
    }

    public T get(int index) {
        if (size() == 0) {
            return null;
        }
        int start = nextFirst + 1;
        if (start >= myArr.length) {
            start = 0;
        }
        index = start + index;
        if (index > myArr.length - 1) {
            index = index - myArr.length;
        }
        return myArr[index];
    }
}
