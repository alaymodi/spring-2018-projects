
import java.util.HashMap;
import java.util.Map;

/**
 * This class provides all code necessary to take a query box and produce
 * a query result. The getMapRaster method must return a Map containing all
 * seven of the required fields, otherwise the front end code will probably
 * not draw the output correctly.
 */
public class Rasterer {

   // private Map<String, Object> results;

    public Rasterer() {
    }

    /**
     * Takes a user query and finds the grid of images that best matches the query. These
     * images will be combined into one big image (rastered) by the front end. <br>
     *
     *     The grid of images must obey the following properties, where image in the
     *     grid is referred to as a "tile".
     *     <ul>
     *         <li>The tiles collected must cover the most longitudinal distance per pixel
     *         (LonDPP) possible, while still covering less than or equal to the amount of
     *         longitudinal distance per pixel in the query box for the user viewport size. </li>
     *         <li>Contains all tiles that intersect the query bounding box that fulfill the
     *         above condition.</li>
     *         <li>The tiles must be arranged in-order to reconstruct the full image.</li>
     *     </ul>
     *
     * @param params Map of the HTTP GET request's query parameters - the query box and
     *               the user viewport width and height.
     *
     * @return A map of results for the front end as specified: <br>
     * "render_grid"   : String[][], the files to display. <br>
     * "raster_ul_lon" : Number, the bounding upper left longitude of the rastered image. <br>
     * "raster_ul_lat" : Number, the bounding upper left latitude of the rastered image. <br>
     * "raster_lr_lon" : Number, the bounding lower right longitude of the rastered image. <br>
     * "raster_lr_lat" : Number, the bounding lower right latitude of the rastered image. <br>
     * "depth"         : Number, the depth of the nodes of the rastered image <br>
     * "query_success" : Boolean, whether the query was able to successfully complete; don't
     *                    forget to set this to true on success! <br>
     */
    public Map<String, Object> getMapRaster(Map<String, Double> params) {
        Map<String, Object> results = new HashMap<>();
        double desRes = calcLonDPP(params.get("ullon"), params.get("lrlon"), params.get("w"));
        boolean querySuccess = validateParams(params);
        if (!querySuccess) {
            return creatEmptyMap(results);
        }

        int depth = calcDepth(desRes);       //calculates depth of the images needed
        //calculates x-coordinate and y-coordinates of first & last image
        int xStartCoord = getXCoordinateofImage(params.get("ullon"), depth);
        int xEndCoord = getXCoordinateofImage(params.get("lrlon"), depth);
        int yStartCoord = getYCoordinateofImage(params.get("ullat"), depth);
        int yEndCoord = getYCoordinateofImage(params.get("lrlat"), depth);
        String [][] images = renderGrid((int) depth, xStartCoord, xEndCoord,
                yStartCoord, yEndCoord);

        double horizSideLength = getHorizontalTileSideLength(depth);
        double vertSideLength = getVerticalTileSideLength(depth);

        results.put("render_grid", images);
        results.put("raster_ul_lon", MapServer.ROOT_ULLON + (xStartCoord * horizSideLength));
        results.put("raster_lr_lon", MapServer.ROOT_ULLON + (xEndCoord + 1) * horizSideLength);
        results.put("raster_ul_lat", MapServer.ROOT_ULLAT - (yStartCoord * vertSideLength));
        results.put("raster_lr_lat", MapServer.ROOT_ULLAT - (yEndCoord + 1) * vertSideLength);
        results.put("depth", (int) depth);
        results.put("query_success", true);
        return results;
    }

    private Map<String, Object> creatEmptyMap(Map<String, Object> results) {
        String [][] images = null;
        results.put("render_grid", images);
        results.put("raster_ul_lon", 0);
        results.put("raster_lr_lon", 0);
        results.put("raster_ul_lat", 0);
        results.put("raster_lr_lat", 0);
        results.put("depth", (int) 0);
        results.put("query_success", false);
        return results;
    }

    private boolean validateParams(Map<String, Double> params) {
        if (!checkOutOfbounds(params)) {
            return false;
        }
        if (!checkOrderOfQueryBox(params)) {
            return false;
        }
        adjustQueryBox(params);
        return true;
    }

    private void adjustQueryBox(Map<String, Double> params) {
        if (MapServer.ROOT_LRLAT > params.get("lrlat")) {
            params.replace("lrlon", MapServer.ROOT_LRLAT);
        }
        if (MapServer.ROOT_ULLAT < params.get("ullat")) {
            params.replace("ullat", MapServer.ROOT_ULLAT);
        }
        if (MapServer.ROOT_ULLON > params.get("ullon")) {
            params.replace("ullon", MapServer.ROOT_ULLON);
        }
        if (MapServer.ROOT_LRLON < params.get("lrlon")) {
            params.replace("lrlon", MapServer.ROOT_LRLON);
        }
    }

    private boolean checkOutOfbounds(Map<String, Double> params) {
        if (params.get("lrlon") < MapServer.ROOT_ULLON) {
            return false;
        }
        if (params.get("ullon") > MapServer.ROOT_LRLON) {
            return false;
        }
        if (params.get("ullat") < MapServer.ROOT_LRLAT) {
            return false;
        }
        if (params.get("lrlat") > MapServer.ROOT_ULLAT) {
            return false;
        }
        return true;
    }

    private boolean checkOrderOfQueryBox(Map<String, Double> params) {
        if (params.get("ullon") > params.get("lrlon")) {
            return false;
        }
        if (params.get("ullat") < params.get("lrlat")) {
            return false;
        }
        return true;
    }

    private String [][] renderGrid(int depth, int xStartCoord, int xEndCoord,
                                   int yStartCoord, int yEndCoord) {
        String [][] images = new String[yEndCoord - yStartCoord + 1][xEndCoord - xStartCoord + 1];
        String startStr = "d" + Integer.toString(depth);
        String endStr = ".png";
        for (int row = 0; row < images.length; row++) {
            int xCoord = xStartCoord;
            for (int col = 0; col < images[0].length; col++) {
                images[row][col] = startStr + "_x" + Integer.toString(xCoord)
                        + "_y" + Integer.toString(yStartCoord) + endStr;
                xCoord++;
            }
            yStartCoord++;
        }
        return images;
    }

    private double getHorizontalTileSideLength(int depth) {
        double mapHorizontalDistance = Math.abs(MapServer.ROOT_ULLON - MapServer.ROOT_LRLON);
        double tileSideLength = mapHorizontalDistance / Math.pow(2, depth);
        return tileSideLength;
    }

    private double getVerticalTileSideLength(int depth) {
        double mapVerticalDistance = Math.abs(MapServer.ROOT_ULLAT - MapServer.ROOT_LRLAT);
        double tileSideLength = mapVerticalDistance / Math.pow(2, depth);
        return tileSideLength;
    }

    //returns x-coordinate of tile
    private int getXCoordinateofImage(double lon, int depth) {
        //calculates what the max x-coordinate of tile
        int maxTiles = (int) Math.pow(2, depth);
        double tileSideLength = getHorizontalTileSideLength(depth);
        double xDist = Math.abs(MapServer.ROOT_ULLON - lon);
        int tileNum = (int) (xDist / tileSideLength);
        if (tileNum >= maxTiles) {
            return maxTiles - 1;
        } else {
            return tileNum;
        }

    }

    //returns y-coordinate of tile
    private int getYCoordinateofImage(double lat, int depth) {
        int maxTiles = (int) Math.pow(2, depth);
        double tileSideLength = getVerticalTileSideLength(depth);
        double yDist = Math.abs(MapServer.ROOT_ULLAT - lat);
        int tileNum = (int) (yDist / tileSideLength);
        if (tileNum >= maxTiles) {
            return maxTiles - 1;
        } else {
            return tileNum;
        }
    }

    private double calcLonDPP(double ullon, double lrlon,  double width) {
        return Math.abs(ullon - lrlon) / width;
    }

    private int calcDepth(double desResoultion) {
        double img0Resolution = calcLonDPP(MapServer.ROOT_ULLON,
                MapServer.ROOT_LRLON, MapServer.TILE_SIZE);
        if (desResoultion > img0Resolution) {
            return 0;
        }
        double depthXres = 0;
        for (int x = 1; x < 8; x++) {
            depthXres = (img0Resolution) / (Math.pow(2, x));
            if (depthXres <= desResoultion) {
                return x;          //returns resolution and depth of that tile
            }
        }
        return 7;
    }
}
