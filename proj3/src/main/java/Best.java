import java.util.Map;
import java.util.HashMap;

public class Best {

    Map<Long, Long> parents;        //child is key and parent is value
    Map<Long, Double> distance;     //distance from start to vertex

    public Best(Long l) {
        parents = new HashMap<>();
        distance = new HashMap<>();
        distance.put(l, 0.0);
    }


    public void put(Long childId, Long parentId, Double dist) {
        if (distance.containsKey(childId)) {
            if (distance.get(childId) > dist) {
                //if the child is already in the map, replace its distance and parent
                distance.replace(childId, dist);
                parents.replace(childId, parentId);
            }
        } else {
            distance.put(childId, dist);
            parents.put(childId, parentId);
        }
    }

}
