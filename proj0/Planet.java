public class Planet{
	public double xxPos;
	public double yyPos;
	public double xxVel;
	public double yyVel;
	public double mass;
	public String imgFileName; 
	
	public Planet(double xP, double yP, double xV,
              double yV, double m, String img)
	{
		xxPos = xP;
		yyPos = yP;
		xxVel = xV;
		yyVel = yV;
		mass = m;
		imgFileName = img;

	}
	public Planet(Planet p){
		xxPos = p.xxPos;
		yyPos = p.yyPos;
		xxVel = p.xxVel;
		yyVel = p.yyVel;
		mass = p.mass;
		imgFileName = p.imgFileName;

	}
	public double calcDistance(Planet p){
		double dx = xxPos - p.xxPos;
		double dy = yyPos - p.yyPos;
		return Math.pow(((dx*dx) + (dy*dy)), 0.5);
	}

	public double calcForceExertedBy(Planet p){
		double r = calcDistance(p);
		double G = 6.67 * Math.pow(10,-11);

		return (G* mass * p.mass)/(r*r);
	}

	public double calcForceExertedByX(Planet p){
		double dx = p.xxPos - xxPos;
		return (calcForceExertedBy(p) * dx)/calcDistance(p);
	}

	public double calcForceExertedByY(Planet p){
		double dy = p.yyPos - yyPos;
		return (calcForceExertedBy(p) * dy)/calcDistance(p);
	}
	public double calcNetForceExertedByX(Planet [] p){
		double net_force = 0;
		for(int len = 0; len<p.length;len++){			
			if(!this.equals(p[len])){
				net_force+=calcForceExertedByX(p[len]);
			}
		}
		return net_force;
	}

	public double calcNetForceExertedByY(Planet [] p){
		double net_force = 0;
		for(int len = 0; len<p.length;len++){
			if(!this.equals(p[len])){
				net_force+=calcForceExertedByY(p[len]);
			}
		}
		return net_force;
	}
	public void update(double dt, double xF, double yF){
		double aX = xF/mass;
		double aY = yF/mass;
		xxVel = xxVel + dt * aX;
		yyVel = yyVel + dt * aY;
		xxPos = xxPos + dt * xxVel;
		yyPos = yyPos + dt * yyVel;
	}
	public void draw(){
		StdDraw.picture(xxPos,yyPos,"images/" + imgFileName);
	}
}