public class NBody{

	public static double readRadius(String FName){
		In in = new In(FName);
		int num_planets = in.readInt();
		double radius = in.readDouble();
		return radius;
	}
	public static Planet [] readPlanets(String FName){
		In in = new In(FName);
		int num_planets = in.readInt();
		double radius = in.readDouble();
		Planet [] plans = new Planet[num_planets];
		for(int len = 0; len<num_planets;len++){
			double xP = in.readDouble();
			double yP = in.readDouble();
			double xV = in.readDouble();
			double yV = in.readDouble();
			double m = in.readDouble();
			String FileName = in.readString();
			plans[len] = new Planet(xP, yP, xV, yV, m, FileName);
		}
		return plans;
	}

	public static void main (String [] args){
		double T = Double.parseDouble(args[0]);
		double dT = Double.parseDouble(args[1]);
		String filename = args[2];
		double radius = NBody.readRadius(filename);
		Planet [] plans = NBody.readPlanets(filename);
		StdDraw.setScale(-radius,radius);
		StdDraw.picture(0,0, "images/starfield.jpg");
		
		for(int len =0;len<plans.length;len++){
			plans[len].draw();
		}
		// StdDraw.show();
		StdDraw.enableDoubleBuffering();
		double time  =0;
		double [] xForces = new double[plans.length];
		double [] yForces = new double[plans.length];
		while(time<T){
			
		}


	}
}